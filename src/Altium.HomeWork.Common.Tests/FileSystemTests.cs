using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using FluentAssertions;
using Xunit;

namespace Altium.HomeWork.Common.Tests
{
    public class FileSystemTests
    {
        [Theory]
        [InlineData("this is a string", 16)]
        [InlineData("это строка", 19)]
        public async Task Size_count_test(string str, int expectedCount)
        {
            var fi = new FileInfo("test.file");
            using var writer = FileSystem.CreateWriter(fi);
            await writer.WriteLineAsync(str, CancellationToken.None);
            writer.FileSize.Should().Be(expectedCount + Environment.NewLine.Length);
            FileSystem.SafeDeleteFile(fi);
        }
    }
}