using System;
using System.Collections.Generic;
using FluentAssertions;
using Xunit;

namespace Altium.HomeWork.Common.Tests
{
    public class AltiumValueTests
    {
        public static IEnumerable<object[]> CompareTestsData => new List<object[]>
        {
            new object[] {AltiumValue.Parse("1. Apple"), AltiumValue.Parse("1. Apple"), 0},
            new object[] {AltiumValue.Parse("2. Apple"), AltiumValue.Parse("1. Apple"), 1},
            new object[] {AltiumValue.Parse("1. Apple"), AltiumValue.Parse("2. Apple"), -1},
            new object[] {AltiumValue.Parse("1. Apple"), AltiumValue.Parse("1. Apples"), -1},
            new object[] {AltiumValue.Parse("1. Apples"), AltiumValue.Parse("1. Apple"), 1},
            new object[] {AltiumValue.Parse("1. Apples"), AltiumValue.Parse("2. Apple"), 1}
        };

        public static IEnumerable<object[]> EqualTestsData => new List<object[]>
        {
            new object[] {AltiumValue.Parse("1. Apple"), AltiumValue.Parse("1. Apple"), true},
            new object[] {AltiumValue.Parse("1. Apple"), AltiumValue.Parse("1.   Apple"), false},
            new object[] {AltiumValue.Parse("2. Apple"), AltiumValue.Parse("1. Apple"), false},
            new object[] {AltiumValue.Parse("1. Apple"), AltiumValue.Parse("2. Apple"), false},
            new object[] {AltiumValue.Parse("1. Apple"), AltiumValue.Parse("1. Apples"), false},
            new object[] {AltiumValue.Parse("1. Apples"), AltiumValue.Parse("1. Apple"), false}
        };

        [Theory]
        [MemberData(nameof(CompareTestsData))]
        public void Compare_tests(AltiumValue x, AltiumValue y, int expected)
        {
            if (expected < 0)
            {
                x.CompareTo(y).Should().BeLessThan(0);
            }

            if (expected > 0)
            {
                x.CompareTo(y).Should().BeGreaterThan(0);
            }

            if (expected == 0)
            {
                x.CompareTo(y).Should().Be(0);
            }
        }

        [Theory]
        [MemberData(nameof(EqualTestsData))]
        public void Equal_tests(AltiumValue x, AltiumValue y, bool expected)
        {
            x.Equals(y).Should().Be(expected);
        }

        [Theory]
        [MemberData(nameof(EqualTestsData))]
        public void HashCode_tests(AltiumValue x, AltiumValue y, bool expected)
        {
            if (expected)
            {
                x.GetHashCode().Should().Be(y.GetHashCode());
            }
        }

        [Theory]
        [InlineData("415. Apple", 415, "Apple")]
        [InlineData("30432. Something something something", 30432, "Something something something")]
        [InlineData("1. Apple", 1, "Apple")]
        [InlineData("32. Cherry is the best", 32, "Cherry is the best")]
        [InlineData("2. Banana is yellow", 2, "Banana is yellow")]
        [InlineData("2.  Banana is yellow", 2, " Banana is yellow")]
        public void Correct_input_parsing(string str, int expectedInteger, string expectedString)
        {
            var value = AltiumValue.Parse(str);
            value.IntPart.Should().Be(expectedInteger);
            value.StringPart.Should().Be(expectedString);
        }

        [Theory]
        [InlineData("Green. Apple")]
        [InlineData("Green.Apple")]
        [InlineData("Green .Apple")]
        [InlineData("123.456")]
        [InlineData("123 .456")]
        [InlineData("2 Banana")]
        [InlineData("2.Banana")]
        [InlineData("2 .Banana")]

        [InlineData("123")]
        [InlineData("123.")]
        [InlineData("Cherry is the best")]
        [InlineData(" Cherry is the best")]
        [InlineData(". Cherry is the best")]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(".")]
        [InlineData(". ")]
        [InlineData(null)]
        public void Incorrect_input_throws_exception(string str)
        {
            Action act = () => AltiumValue.Parse(str);
            act.Should().Throw<FormatException>();
        }

        [Theory]
        [InlineData("Green. Apple")]
        [InlineData("Green.Apple")]
        [InlineData("Green .Apple")]
        [InlineData("123.456")]
        [InlineData("123 .456")]
        [InlineData("2 Banana")]
        [InlineData("2.Banana")]
        [InlineData("2 .Banana")]
        [InlineData("123")]
        [InlineData("123.")]
        [InlineData("Cherry is the best")]
        [InlineData(" Cherry is the best")]
        [InlineData(". Cherry is the best")]
        [InlineData("")]
        [InlineData(" ")]
        [InlineData(".")]
        [InlineData(". ")]
        [InlineData(null)]
        public void Safe_parser_does_not_throw(string str)
        {
            AltiumValue.TryParse(str, out var result).Should().Be(false);
            result.Should().Be(AltiumValue.Default());
        }
    }
}