using System;
using System.Collections.Generic;
using Altium.HomeWork.Common;
using Altium.HomeWork.Sorter.ExternalMergeSort;
using FluentAssertions;
using Xunit;

namespace Altium.HomeWork.Sorter.Tests
{
    public class LookupTests
    {
        [Fact]
        public void AltiumValue_is_sorted_properly()
        {
            var lookup = new Lookup<AltiumValue, string>();
            lookup.Add(new AltiumValue(3, "ccc"), "1");
            lookup.Add(new AltiumValue(3, "ccc"), "2");
            lookup.Add(new AltiumValue(1, "bbb"), "bbb");
            lookup.Add(new AltiumValue(5, "bbb"), "bbb");
            lookup.Add(new AltiumValue(1, "aaa"), "aaa");

            lookup.Pop().Should().BeEquivalentTo((new AltiumValue(1, "aaa"), new List<string> {"aaa"}));
            lookup.Pop().Should().BeEquivalentTo((new AltiumValue(1, "bbb"), new List<string> {"bbb"}));
            lookup.Pop().Should().BeEquivalentTo((new AltiumValue(5, "bbb"), new List<string> {"bbb"}));
            lookup.Pop().Should().BeEquivalentTo((new AltiumValue(3, "ccc"), new List<string> {"1", "2"}));
            lookup.Any().Should().BeFalse();
        }

        [Fact]
        public void Any_works_properly()
        {
            var lookup = new Lookup<int, string>();
            lookup.Any().Should().BeFalse();
            lookup.Add(4, "four");
            lookup.Any().Should().BeTrue();
            lookup.Add(2, "two");
            lookup.Any().Should().BeTrue();
            lookup.Pop();
            lookup.Any().Should().BeTrue();
            lookup.Pop();
            lookup.Any().Should().BeFalse();
        }

        [Fact]
        public void Empty_lookup_throws_when_pop()
        {
            var lookup = new Lookup<int, string>();
            lookup.Add(4, "four");
            lookup.Pop();
            Action act = () => lookup.Pop();
            act.Should().Throw<ArgumentOutOfRangeException>();
        }

        [Fact]
        public void Lookup_groups_values_by_the_keys()
        {
            var lookup = new Lookup<int, string>();
            lookup.Add(4, "four");
            lookup.Add(5, "five");
            lookup.Add(5, "five and a half");
            lookup.Add(2, "two");
            lookup.Pop().Should().BeEquivalentTo((2, new List<string> {"two"}));
            lookup.Pop().Should().BeEquivalentTo((4, new List<string> {"four"}));
            lookup.Pop().Should().BeEquivalentTo((5, new List<string> {"five", "five and a half"}));
        }

        [Fact]
        public void Lookup_sorts_keys()
        {
            var lookup = new Lookup<int, string>();
            lookup.Add(4, "four");
            lookup.Add(5, "five");
            lookup.Add(2, "two");
            lookup.Pop().Should().BeEquivalentTo((2, new List<string> {"two"}));
            lookup.Pop().Should().BeEquivalentTo((4, new List<string> {"four"}));
            lookup.Pop().Should().BeEquivalentTo((5, new List<string> {"five"}));
        }
    }
}