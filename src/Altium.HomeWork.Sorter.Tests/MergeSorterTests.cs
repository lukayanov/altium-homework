using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Altium.HomeWork.Common;
using FluentAssertions;
using Xunit;

namespace Altium.HomeWork.Sorter.Tests
{
    public class MergeSorterTests
    {
        public class DisposableTempFile : IDisposable
        {
            public FileInfo Value { get; private set; }

            public DisposableTempFile()
            {
                Value = new FileInfo(Path.GetTempFileName());
            }
            public void Dispose()
            {
                FileSystem.SafeDeleteFile(Value);
            }
        }
        
        [Fact]
        public async Task Bad_format_does_not_throw_if_ignored()
        {
            using var inputFile = new DisposableTempFile();
            using var outputFile = new DisposableTempFile();
            await FileSystem.WriteLinesAsync(inputFile.Value, new[] {"123.Apple"}, CancellationToken.None);
            await ExternalMergeSort.Sorter.SortAsync(inputFile.Value, outputFile.Value, 1000000000, true, CancellationToken.None);
            outputFile.Value.Length.Should().Be(3); // BOM header
        }

        [Fact]
        public async Task Bad_format_throws_if_not_ignored()
        {
            using var inputFile = new DisposableTempFile();
            await FileSystem.WriteLinesAsync(inputFile.Value, new[] {"123.Apple"}, CancellationToken.None);
            Func<Task> act = async () =>
                await ExternalMergeSort.Sorter.SortAsync(inputFile.Value, new FileInfo(Path.GetTempFileName()), 1000000000, false, CancellationToken.None);
            act.Should().Throw<FormatException>();
        }

        [Fact]
        public void Empty_input_file_is_valid()
        {
            Func<Task> act = async () =>
                await ExternalMergeSort.Sorter.SortAsync(new FileInfo(Path.GetTempFileName()), new FileInfo(Path.GetTempFileName()), 1000000000, false, CancellationToken.None);
            act.Should().NotThrow<FileNotFoundException>();
        }

        [Fact]
        public void Missing_input_file_is_invalid()
        {
            Func<Task> act = async () =>
                await ExternalMergeSort.Sorter.SortAsync(new FileInfo(Guid.NewGuid().ToString()), new FileInfo(Guid.NewGuid().ToString()), 1000000000, false, CancellationToken.None);
            act.Should().Throw<FileNotFoundException>();
        }
        
        [Fact]
        public async Task Sorting_is_done_correctly()
        {
            using var inputFile = new DisposableTempFile();
            using var outputFile = new DisposableTempFile();
            await FileSystem.WriteLinesAsync(inputFile.Value, new[]
            {
                "415. Apple",
                "30432. Something something something",
                "1. Apple",
                "32. Cherry is the best",
                "2. Banana is yellow"
            }, CancellationToken.None);
            
            await ExternalMergeSort.Sorter.SortAsync(inputFile.Value, outputFile.Value, 1000000000, false, CancellationToken.None);
            var sorted = await File.ReadAllLinesAsync(outputFile.Value.FullName);
            sorted.Should().BeEquivalentTo(new []
            {
                "1. Apple",
                "415. Apple",
                "2. Banana is yellow",
                "32. Cherry is the best",
                "30432. Something something something"
            });
        }
    }
}