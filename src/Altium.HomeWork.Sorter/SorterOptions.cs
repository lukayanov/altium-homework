using System.IO;

namespace Altium.HomeWork.Sorter
{
    public class SorterOptions
    {
        public FileInfo InputFileName { get; set; }
        public FileInfo OutputFileName { get; set; }
        public long MemoryToUse { get; set; }
        public bool AllowFormatErrors { get; set; }
    }
}