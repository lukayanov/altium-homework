﻿using System;
using System.CommandLine;
using System.CommandLine.Builder;
using System.CommandLine.Hosting;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Altium.HomeWork.Common;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Serilog;
using Serilog.Events;

namespace Altium.HomeWork.Sorter
{
    public static class Program
    {
        public static async Task<int> Main(string[] args)
        {
            CreateLogger();
            // System.CommandLine.Hosting package is used here to build the app.
            // This package is still in alpha stage (0.3.0-alpha.20371.2), I believe it should not be a problem for a test project  
            var resultCode = await BuildCommandLine()
                .UseHost(BuildHost)
                .UseDefaults()
                .CancelOnProcessTermination()
                .UseExceptionHandler((ex, c) =>
                {
                    c.ResultCode = -1;
                    Log.Fatal(ex, "Unexpected error has occured, aborting.");
                })
                .Build()
                .InvokeAsync(args);
            return resultCode;
        }

        private static CommandLineBuilder BuildCommandLine()
        {
            var generateCommand = new RootCommand(@"Sort test file content")
            {
                new Option<FileInfo>(new[] {"--input-file-name", "-ifn"})
                {
                    IsRequired = true,
                    Description = "Name of the file to sort"
                }.LegalFilePathsOnly(),
                new Option<FileInfo>(new[] {"--output-file-name", "-ofn"})
                {
                    IsRequired = true,
                    Description = "Name of the file to store the result"
                }.LegalFilePathsOnly(),
                new Option<long>(new[] {"--memory-to-use", "-mu"}, () => 100000000)
                {
                    IsRequired = false,
                    Description = "Size of the memory buffer for sorting, in bytes. Should be at least twice as max string length, [2, 200000000000]."
                }.AllowedRange(1, 200000000000),
                new Option<bool>(new[] {"--allow-format-errors", "-afe"}, () => false)
                {
                    IsRequired = false,
                    Description = "Allow format errors in input file."
                }
            };

            generateCommand.Handler = CommandHandler.Create<IHost, CancellationToken>(CommandAction);
            return new CommandLineBuilder(generateCommand);
        }

        private static async Task<int> CommandAction(IHost host, CancellationToken token)
        {
            var options = host.Services.GetRequiredService<IOptions<SorterOptions>>().Value;
            try
            {
                Log.Information("Begin sorting.");
                await ExternalMergeSort.Sorter.SortAsync(options.InputFileName, options.OutputFileName, options.MemoryToUse, options.AllowFormatErrors, token);
                Log.Information("Sorting is complete.");

                return 0;
            }
            catch (FormatException e)
            {
                Log.Error(e, "Bad input format, existing");
            }
            catch (TaskCanceledException)
            {
                Log.Information("Generation is cancelled, exiting");
            }
            catch (FileNotFoundException e)
            {
                Log.Error(e, "File not found, exiting");
            }
            catch (IOException e)
            {
                Log.Error(e, "Failed to generate the file");
            }
            catch (ArgumentException e)
            {
                Log.Error(e, "Failed to generate the file");
            }
            return -1;            
        }

        private static void CreateLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss} [{Level}] ({SourceContext}) {Message}{NewLine}{Exception}")
                .CreateLogger();
        }

        private static void BuildHost(IHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services.AddOptions<SorterOptions>().BindCommandLine();
            }).UseSerilog();
        }
    }
}