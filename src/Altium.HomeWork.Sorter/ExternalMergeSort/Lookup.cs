using System;
using System.Collections.Generic;

namespace Altium.HomeWork.Sorter.ExternalMergeSort
{
    // A lookup class allowing to find the smallest value over all files
    internal class Lookup<TKey, TValue>
    {
        private readonly SortedList<TKey, List<TValue>> _lookup = new SortedList<TKey, List<TValue>>();

        public bool Any()
        {
            return _lookup.Count > 0;
        }

        public void Add(TKey key, TValue value)
        {
            if (_lookup.TryGetValue(key, out var list))
            {
                list.Add(value);
            }
            else
            {
                if (!_lookup.TryAdd(key, new List<TValue> {value}))
                {
                    throw new InvalidOperationException();
                }
            }
        }

        public (TKey, List<TValue>) Pop()
        {
            var result = (_lookup.Keys[0], _lookup.Values[0]);
            _lookup.RemoveAt(0);
            return result;
        }
    }
}