using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Altium.HomeWork.Common;
using Serilog;

namespace Altium.HomeWork.Sorter.ExternalMergeSort
{
    public static class Sorter
    {
        private static readonly ILogger Logger = Log.ForContext(typeof(Sorter));

        public static async Task SortAsync(FileInfo inputFile, FileInfo outputFile, long memoryToUse, bool ignoreFormatErrors, CancellationToken token)
        {
            Logger.Information("Begin splitting file to sorted chunks");
            var files = await SorterSplit.SplitFileAsync(inputFile, memoryToUse, ignoreFormatErrors, token);
            Logger.Information("Splitting is completed");
            try
            {
                Logger.Information("Begin merging sorted chunks");
                await Merger.MergeFilesAsync(outputFile, files, memoryToUse, token);
                Logger.Information("Merge is completed");
            }
            catch (Exception)
            {
                FileSystem.SafeDeleteFile(outputFile);
                throw;
            }
            finally
            {
                files.Select(t => t.file).ToList().ForEach(FileSystem.SafeDeleteFile);
            }
        }
    }
}