using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Altium.HomeWork.Common;
using Serilog;

namespace Altium.HomeWork.Sorter.ExternalMergeSort
{
    // Merger part of the sorter. This code is static, it is put to the separate class/file just to make things simpler
    internal static class Merger
    {
        private static readonly ILogger Logger = Log.ForContext(typeof(Merger));

        public static async Task MergeFilesAsync(FileInfo outputFile, ICollection<(FileInfo file, long linesCount)> filesToMerge, long memoryToUse, CancellationToken token)
        {
            // Algorithm executes merge even if there is only one sorted file in the input. This is a potential optimization
            var queues = await Task.WhenAll(filesToMerge.Select(t => FileBasedQueue.CreateAsync(t.file, memoryToUse / filesToMerge.Count, token)));
            try
            {
                var totalLines = filesToMerge.Aggregate(0L, (l, t) => l + t.linesCount);
                var sortedStrings = MergeProjections(queues, totalLines, token);
                await FileSystem.WriteLinesAsync(outputFile, sortedStrings, token);
            }
            finally
            {
                await Task.WhenAll(queues.Select(async p => await p.DisposeAsync()));
            }
        }

        private static async IAsyncEnumerable<string> MergeProjections(IEnumerable<FileBasedQueue> queues, long totalLines, [EnumeratorCancellation] CancellationToken token)
        {
            var counter = 0L;
            var lookup = new Lookup<AltiumValue, FileBasedQueue>();
            foreach (var (nv, queue) in queues.Select(p => (nv: p.Peek(), queue: p)).Where(p => p.nv.HasValue))
            {
                lookup.Add(nv.Value, queue);
            }

            while (lookup.Any())
            {
                // parallel execution
                var (value, valueQueues) = lookup.Pop();
                foreach (var q in valueQueues)
                {
                    yield return value.ToString();
                    await q.PopAsync(token);
                    var next = q.Peek();
                    if (next.HasValue)
                    {
                        lookup.Add(next.Value, q);
                    }

                    Logger.UpdateProgress("Merging", counter, ++counter, totalLines);
                }
            }
        }
    }
}