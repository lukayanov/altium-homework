using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Altium.HomeWork.Common;
using Serilog;

namespace Altium.HomeWork.Sorter.ExternalMergeSort
{
    // Splitter part of the sorter. This code is static, it is put to the separate class/file just to make things simpler
    internal static class SorterSplit
    {
        private static readonly ILogger Logger = Log.ForContext(typeof(SorterSplit));

        public static async Task<ICollection<(FileInfo file, long linesCount)>> SplitFileAsync(FileInfo file, long memoryToUse, bool ignoreFormatErrors, CancellationToken token)
        {
            if (!file.Exists)
            {
                throw new FileNotFoundException();
            }

            var stream = FileSystem.ReadLines(file);
            var chunks = ConvertAndSplitBySize(stream, memoryToUse, ignoreFormatErrors, token);
            return await SortAndSaveChunksAsync(chunks, token);
        }

        private static AltiumValue? Convert(string s, bool ignoreFormatErrors)
        {
            return !ignoreFormatErrors ? AltiumValue.Parse(s) : AltiumValue.TryParse(s, out var result) ? new AltiumValue?(result) : null;
        }

        private static async Task<ICollection<(FileInfo file, long linesCount)>> SortAndSaveChunksAsync(IAsyncEnumerable<List<AltiumValue>> chunkStream, CancellationToken token)
        {
            var result = new List<(FileInfo, long)>();
            await foreach (var chunk in chunkStream.WithCancellation(token))
            {
                chunk.Sort();
                var fileInfo = new FileInfo(Path.GetTempFileName());
                try
                {
                    Logger.Debug($"Saving file {fileInfo.FullName}");
                    var linesCount = await FileSystem.WriteLinesAsync(fileInfo, chunk.Select(i => i.ToString()), token);
                    result.Add((fileInfo, linesCount));
                }
                catch (Exception)
                {
                    FileSystem.SafeDeleteFile(fileInfo);
                    result.Select(t => t.Item1).ToList().ForEach(FileSystem.SafeDeleteFile);
                    throw;
                }
            }

            return result;
        }

        private static async IAsyncEnumerable<List<AltiumValue>> ConvertAndSplitBySize(IAsyncEnumerable<string> stream, long memoryToUse, bool ignoreFormatErrors,
            [EnumeratorCancellation] CancellationToken token)
        {
            var result = new List<AltiumValue>();
            var currentSize = 0L;
            await foreach (var value in stream.WithCancellation(token))
            {
                var nullable = Convert(value, ignoreFormatErrors);
                if (nullable.HasValue)
                {
                    var converted = nullable.Value;
                    result.Add(converted);
                    currentSize += converted.Size;
                    if (currentSize > memoryToUse)
                    {
                        yield return result;
                        result = new List<AltiumValue>();
                        currentSize = 0;
                    }
                }
            }

            yield return result;
        }
    }
}