using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Altium.HomeWork.Common;

namespace Altium.HomeWork.Sorter.ExternalMergeSort
{
    // This class is some kind of a queue, providing serial access to the file contents.    
    internal class FileBasedQueue : IAsyncDisposable
    {
        private readonly long _memoryToUse;
        private readonly Queue<AltiumValue> _queue = new Queue<AltiumValue>();
        private IAsyncEnumerator<string> _asyncIterator;

        private FileBasedQueue(FileInfo file, long memoryToUse)
        {
            _memoryToUse = memoryToUse;
            _asyncIterator = FileSystem.ReadLines(file).GetAsyncEnumerator();
        }

        public async ValueTask DisposeAsync()
        {
            if (_asyncIterator != null)
            {
                await _asyncIterator.DisposeAsync();
            }
        }

        public static async Task<FileBasedQueue> CreateAsync(FileInfo file, long memoryToUse, CancellationToken token)
        {
            var obj = new FileBasedQueue(file, memoryToUse);
            await obj.LoadDataAsync(token); // Load queue 
            return obj;
        }

        public AltiumValue? Peek()
        {
            return _queue.TryPeek(out var val) ? val : (AltiumValue?) null;
        }

        public async Task PopAsync(CancellationToken token)
        {
            _queue.TryDequeue(out _);
            if (_queue.Count == 0)
            {
                await LoadDataAsync(token);
            }
        }

        private async Task LoadDataAsync(CancellationToken token)
        {
            if (_asyncIterator == null)
            {
                return;
            }

            var size = 0L;
            while (size < _memoryToUse)
            {
                if (await _asyncIterator.MoveNextAsync(token))
                {
                    var value = AltiumValue.Parse(_asyncIterator.Current);
                    size += value.Size;
                    _queue.Enqueue(value);
                }
                else
                {
                    await _asyncIterator.DisposeAsync();
                    _asyncIterator = null;
                    break;
                }
            }
        }
    }
}