﻿using System.CommandLine;
using System.CommandLine.Builder;
using System.CommandLine.Hosting;
using System.CommandLine.Invocation;
using System.CommandLine.Parsing;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Altium.HomeWork.Common;
using Altium.HomeWork.Generator.StringSource;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;

namespace Altium.HomeWork.Generator
{
    public static class Program
    {
        public static async Task<int> Main(string[] args)
        {
            var logger = CreateLogger();
            // System.CommandLine.Hosting package is used here to build the app.
            // This package is still in alpha stage (0.3.0-alpha.20371.2), I believe it should not be a problem for a test project  
            var resultCode = await BuildCommandLine()
                .UseHost(BuildHost)
                .UseDefaults()
                .CancelOnProcessTermination()
                .UseExceptionHandler((ex, c) =>
                {
                    c.ResultCode = -1;
                    logger.Fatal(ex, "Unexpected error has occured, aborting.");
                })
                .Build()
                .InvokeAsync(args);
            return resultCode;
        }

        private static CommandLineBuilder BuildCommandLine()
        {
            var generateCommand = new RootCommand(@"Generate test file")
            {
                new Option<FileInfo>(new[] {"--output-file-name", "-ofn"})
                {
                    IsRequired = true,
                    Description = "Name of the file to generate"
                }.LegalFilePathsOnly(),
                new Option<long>(new[] {"--file-size", "-fs"}, () => 1000000000)
                {
                    IsRequired = false,
                    Description = "Size of the file to generate, in bytes. [0, 200000000000]."
                }.AllowedRange(0, 200000000000),
                new Option<int>(new[] {"--dictionary-size", "-ds"}, () => 1000000)
                {
                    IsRequired = false,
                    Description = $"Number of words in the dictionary that is used to generate file. [1, {int.MaxValue}]"
                }.AllowedRange(1, int.MaxValue),
                new Option<int>(new[] {"--max-string-length", "-ml"}, () => 1024)
                {
                    IsRequired = false,
                    Description = "Maximal length of string in dictionary. [1, 1024]"
                }.AllowedRange(1, 1024)
            };
            generateCommand.AddValidator(c =>
            {
                // We need to make sure that duplicate strings are saved to the output file
                // Thus we need to have dictionary size less than number of lines in the output file.
                // The file size is defined in bytes not in lines.
                // However we may estimate the average length of the string, taking into consideration the even distribution of random length of the strings
                var fileSize = c["-fs"]?.GetValueOrDefault<long>();
                var dictSize = c["-ds"]?.GetValueOrDefault<int>();
                var maxStrLength = c["-ml"]?.GetValueOrDefault<int>();
                var estimatedNumberOfLines = fileSize / ((maxStrLength + 1) / 2);
                if (estimatedNumberOfLines < dictSize)
                {
                    return $"Estimated number of lines in the output file is {estimatedNumberOfLines}\n" +
                           $"This is less than number of lines in the dictionary: {dictSize}\n" +
                           "To make sure the result file contains duplicated strings please reduce the dictionary size or increase the output file size.";
                }

                return null;
            });

            generateCommand.Handler = CommandHandler.Create<IHost, CancellationToken>(async (host, token) =>
            {
                var success = await host.Services.GetRequiredService<GeneratorService>().GenerateAsync(token);
                return success ? 0 : -1; // Converting result to exit code standard
            });
            return new CommandLineBuilder(generateCommand);
        }

        private static ILogger CreateLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.Console(outputTemplate: "{Timestamp:yyyy-MM-dd HH:mm:ss} [{Level}] ({SourceContext}) {Message}{NewLine}{Exception}")
                .CreateLogger();
            return Log.ForContext(typeof(Program));
        }

        private static void BuildHost(IHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                services.AddOptions<GeneratorOptions>().BindCommandLine();
                services.AddOptions<DictionaryOptions>().BindCommandLine();

                services.AddTransient<GeneratorService>();
                services.AddTransient<IStringDictionary, RandomStringDictionary>();
                services.AddTransient<IStringSource, RandomStringSource>();
            }).UseSerilog();
        }
    }
}