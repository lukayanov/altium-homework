using System;
using System.Collections.Generic;

namespace Altium.HomeWork.Generator.StringSource
{
    internal class RandomStringSource : IStringSource
    {
        private readonly IStringDictionary _dictionary;

        public RandomStringSource(IStringDictionary dictionary)
        {
            _dictionary = dictionary;
        }

        public IEnumerable<string> GetStrings()
        {
            var random = new Random();
            while (true)
            {
                yield return $"{random.Next()}. {_dictionary.GetItem(random.Next(_dictionary.Size - 1))}";
            }

            // ReSharper disable once IteratorNeverReturns
        }
    }
}