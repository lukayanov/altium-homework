using System;
using System.Text;
using Microsoft.Extensions.Options;

namespace Altium.HomeWork.Generator.StringSource
{
    // This class has a string dictionary interface.
    // Instead of storing a real dictionary this class uses index based seed. This allows to produce the same random string for the given index.
    // low memory consumption, ability to provide emulate very big dictionaries.  
    internal class RandomStringDictionary : IStringDictionary
    {
        private static readonly char[] AvailableCharacters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz ".ToCharArray();
        private readonly DictionaryOptions _options;
        private readonly int _seed = Environment.TickCount;

        public RandomStringDictionary(IOptions<DictionaryOptions> options)
        {
            _options = options.Value;
            Size = _options.DictionarySize;
        }

        public int Size { get; }

        public string GetItem(int index)
        {
            if (index < 0 || index >= Size)
            {
                throw new IndexOutOfRangeException();
            }

            // Index based seed is used here to produce the same random word for the same index, no real in-memory dictionary is required.  
            var rand = new Random(unchecked(index + _seed));
            var length = rand.Next(1, _options.MaxStringLength);

            var builder = new StringBuilder(length);
            for (var i = 0; i < length;)
            {
                var ch = AvailableCharacters[rand.Next(AvailableCharacters.Length)];
                if (i == 0 && ch == ' ')
                {
                    // Skipping spaces at the beginning of the line to improve readability of the text 
                    continue;
                }

                builder.Append(ch);
                ++i;
            }

            return builder.ToString();
        }
    }
}