namespace Altium.HomeWork.Generator.StringSource
{
    public class DictionaryOptions
    {
        public int DictionarySize { get; set; }
        public int MaxStringLength { get; set; }
    }
}