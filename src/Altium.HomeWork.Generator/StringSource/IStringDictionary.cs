namespace Altium.HomeWork.Generator.StringSource
{
    public interface IStringDictionary
    {
        int Size { get; }
        string GetItem(int index);
    }
}