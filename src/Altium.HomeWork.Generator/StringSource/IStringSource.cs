using System.Collections.Generic;

namespace Altium.HomeWork.Generator.StringSource
{
    public interface IStringSource
    {
        IEnumerable<string> GetStrings();
    }
}