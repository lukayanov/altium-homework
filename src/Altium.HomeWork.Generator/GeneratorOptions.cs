using System.IO;

namespace Altium.HomeWork.Generator
{
    public class GeneratorOptions
    {
        public FileInfo OutputFileName { get; set; }
        public long FileSize { get; set; }
    }
}