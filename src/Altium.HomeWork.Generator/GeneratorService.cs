using System.IO;
using System.Threading;
using System.Threading.Tasks;
using Altium.HomeWork.Common;
using Altium.HomeWork.Generator.StringSource;
using Microsoft.Extensions.Options;
using Serilog;

namespace Altium.HomeWork.Generator
{
    public class GeneratorService
    {
        private readonly ILogger _logger = Log.ForContext<GeneratorService>();
        private readonly GeneratorOptions _options;
        private readonly IStringSource _source;

        public GeneratorService(IStringSource source, IOptions<GeneratorOptions> options)
        {
            _source = source;
            _options = options.Value;
        }

        public async Task<bool> GenerateAsync(CancellationToken stoppingToken)
        {
            try
            {
                _logger.Information($"Generating file {_options.OutputFileName.Name} of the size {_options.FileSize}");
                using var writer = FileSystem.CreateWriter(_options.OutputFileName);
                foreach (var str in _source.GetStrings())
                {
                    var prevValue = writer.FileSize;
                    await writer.WriteLineAsync(str, stoppingToken);
                    var newValue = writer.FileSize;
                    _logger.UpdateProgress("Generating", prevValue, newValue, _options.FileSize);
                    if (writer.FileSize > _options.FileSize)
                    {
                        break;
                    }
                }

                _logger.Information("Generation of the file is completed successfully");
                return true;
            }
            catch (TaskCanceledException)
            {
                _logger.Information("Generation is cancelled");
            }
            catch (IOException e)
            {
                _logger.Error(e, "Failed to generate the file");
            }

            Cleanup();
            return false;
        }

        private void Cleanup()
        {
            _logger.Debug("Deleting file");
            FileSystem.SafeDeleteFile(_options.OutputFileName);
        }
    }
}