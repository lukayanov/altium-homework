using System;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Altium.HomeWork.Common
{
    // WriteLineAsync with bytes count. Takes into account encoded character length
    public sealed class FileWriter : IFileWriter
    {
        private readonly FileStream _writer;

        internal FileWriter(FileInfo fileName)
        {
            _writer = new FileStream(fileName.FullName, FileMode.Create, FileAccess.Write, FileShare.Read, FileSystem.BufferSize, FileOptions.Asynchronous);
            _writer.Write(Encoding.UTF8.GetPreamble()); // UTF8 BOM
        }

        public long FileSize { get; private set; }

        public async Task WriteLineAsync(string str, CancellationToken token)
        {
            var bytes = Encoding.UTF8.GetBytes(str + Environment.NewLine);
            await _writer.WriteAsync(bytes, token);
            FileSize += bytes.Length;
        }

        public void Dispose()
        {
            _writer.Dispose();
        }
    }
}