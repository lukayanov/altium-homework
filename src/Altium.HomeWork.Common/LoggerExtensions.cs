using System.Diagnostics;
using Serilog;

namespace Altium.HomeWork.Common
{
    public static class LoggerExtensions
    {
        public static void UpdateProgress(this ILogger logger, string message, long prevValue, long newValue, long totalSize)
        {
            var prevPercent = prevValue * 100 / totalSize;
            var newPercent = newValue * 100 / totalSize;
            if (prevPercent != newPercent)
            {
                var p = Process.GetCurrentProcess();
                logger.Information($"{message}, progress {newPercent}%, working set {p.WorkingSet64} bytes, total CPU time {p.TotalProcessorTime.TotalSeconds} seconds.");
            }
        }
    }
}