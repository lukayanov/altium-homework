using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Serilog;

namespace Altium.HomeWork.Common
{
    public interface IFileWriter : IDisposable
    {
        long FileSize { get; }
        Task WriteLineAsync(string str, CancellationToken token);
    }

    // These functions provide string based (IEnumerable<string>, IAsyncEnumerable<string>) API to the file system with FileOptions.Asynchronous flag set on.
    public static class FileSystem
    {
        public const int BufferSize =  1048576; // default value is 4096. 1Mb shows visible performance increase. 10Mb looks the same as 1Mb, so I ended up with 1Mb   
        private static readonly ILogger Logger = Log.ForContext(typeof(FileSystem));

        public static IFileWriter CreateWriter(FileInfo file)
        {
            return new FileWriter(file);
        }

        public static void SafeDeleteFile(FileInfo file)
        {
            try
            {
                File.Delete(file.FullName);
            }
            catch (Exception e)
            {
                Logger.Warning(e, $"Cannot delete file {file}");
                // SafeDelete assumes no-throw 
            }
        }

        public static async IAsyncEnumerable<string> ReadLines(FileInfo file)
        {
            await using var fs = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read, BufferSize, FileOptions.Asynchronous);
            using var sr = new StreamReader(fs, Encoding.UTF8, false);
            while (true)
            {
                var str = await sr.ReadLineAsync();
                if (str != null)
                {
                    yield return str;
                }
                else
                {
                    yield break;
                }
            }
        }

        public static async Task<long> WriteLinesAsync(FileInfo fileName, IEnumerable<string> strings, CancellationToken token)
        {
            long counter = 0;
            await using var fs = new FileStream(fileName.FullName, FileMode.Create, FileAccess.Write, FileShare.Read, BufferSize, FileOptions.Asynchronous);
            await using var sw = new StreamWriter(fs, Encoding.UTF8);
            foreach (var str in strings)
            {
                ++counter;
                await sw.WriteLineAsync(str.AsMemory(), token);
            }

            return counter;
        }

        public static async Task<long> WriteLinesAsync(FileInfo fileName, IAsyncEnumerable<string> strings, CancellationToken token)
        {
            long counter = 0;
            await using var fs = new FileStream(fileName.FullName, FileMode.Create, FileAccess.Write, FileShare.Read, BufferSize, FileOptions.Asynchronous);
            await using var sw = new StreamWriter(fs, Encoding.UTF8);
            await foreach (var str in strings.WithCancellation(token))
            {
                ++counter;
                await sw.WriteLineAsync(str.AsMemory(), token);
            }

            return counter;
        }
    }
}