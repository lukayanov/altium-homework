using System;

namespace Altium.HomeWork.Common
{
    //TODO:  == < > => <=  
    public readonly struct AltiumValue : IComparable<AltiumValue>, IEquatable<AltiumValue>
    {
        private const string SplitString = ". ";

        public AltiumValue(int intPart, string stringPart)
        {
            IntPart = intPart;
            StringPart = stringPart;
        }

        public static AltiumValue Default()
        {
            return new AltiumValue(0, "");
        }

        public int CompareTo(AltiumValue other)
        {
            var strCmp = string.Compare(StringPart, other.StringPart, StringComparison.Ordinal);
            return strCmp != 0 ? strCmp : IntPart.CompareTo(other.IntPart);
        }

        public bool Equals(AltiumValue other)
        {
            return (IntPart, StringPart) == (other.IntPart, other.StringPart);
        }

        public override int GetHashCode()
        {
            return (IntPart, StringPart).GetHashCode();
        }

        public override string ToString()
        {
            return IntPart + SplitString + StringPart;
        }

        public long Size => sizeof(int) + StringPart.Length * sizeof(char);


        public int IntPart { get; }
        public string StringPart { get; }

        public static AltiumValue Parse(string s)
        {
            if (s != null)
            {
                // I given up the idea of using regexp because of poor performance. However, regexps allow more control over string parsing
                // The code below accepts values like: "2 . Banana", "2. ", "2. 4" without errors.
                var parts = s.Split(SplitString);
                if (parts.Length != 2)
                {
                    throw new FormatException("The input string does not pattern \"int. string\"");
                }

                if (int.TryParse(parts[0], out var intValue))
                {
                    return new AltiumValue(intValue, parts[1]);
                }
            }

            throw new FormatException();
        }

        public static bool TryParse(string s, out AltiumValue result)
        {
            try
            {
                result = Parse(s);
                return true;
            }
            catch (Exception)
            {
                result = Default();
                return false;
            }
        }
    }
}