using System;
using System.CommandLine;
using System.CommandLine.Parsing;

namespace Altium.HomeWork.Common
{
    public static class CommandLineBuilderExtensions
    {
        public static Option<T> AllowedRange<T>(this Option<T> option, T min, T max) where T : IComparable<T>
        {
            option.AddValidator(r =>
            {
                var value = r.GetValueOrDefault<T>();
                if (value == null || value.CompareTo(min) < 0 || value.CompareTo(max) > 0)
                {
                    return $"Option {r.Option.Name} must be in the range [{min},{max}], but it is {value}";
                }

                return null;
            });
            return option;
        }
    }
}