using System.Linq;
using Altium.HomeWork.Common;
using Altium.HomeWork.Generator.StringSource;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Xunit;

namespace Altium.HomeWork.Generator.Tests
{
    public class RandomStringSourceTests
    {
        [Theory]
        [InlineData(1000)]
        public void String_format_check(int dictionarySize)
        {
            var source = new RandomStringSource(new RandomStringDictionary(Options.Create(new DictionaryOptions {DictionarySize = dictionarySize, MaxStringLength = 1024})));
            foreach (var str in source.GetStrings().Take(dictionarySize))
            {
                AltiumValue.Parse(str);
            }
        }

        [Theory]
        [InlineData(1000, 1024)]
        public void Strings_should_have_duplicates(int dictionarySize, int collectionLength)
        {
            var source = new RandomStringSource(new RandomStringDictionary(Options.Create(new DictionaryOptions {DictionarySize = dictionarySize, MaxStringLength = 1024})));
            var stringParts = source.GetStrings()
                .Take(collectionLength)
                .Select(AltiumValue.Parse)
                .Select(t => t.StringPart).ToList();
            stringParts.Count.Should().BeGreaterThan(stringParts.Distinct().Count());
        }
    }
}