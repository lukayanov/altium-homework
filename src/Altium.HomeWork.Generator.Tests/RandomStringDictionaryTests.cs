using System;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using Altium.HomeWork.Generator.StringSource;
using FluentAssertions;
using Microsoft.Extensions.Options;
using Xunit;

namespace Altium.HomeWork.Generator.Tests
{
    public class RandomStringDictionaryTests
    {
        [Theory]
        [InlineData(11, 10)]
        [InlineData(-1, 40)]
        [InlineData(1, 1)]
        public void Invalid_index_test(int index, int dictionarySize)
        {
            var dictionary = new RandomStringDictionary(Options.Create(new DictionaryOptions {DictionarySize = dictionarySize, MaxStringLength = 100}));
            Action act = () => dictionary.GetItem(index);
            act.Should().Throw<IndexOutOfRangeException>();
        }

        [Theory]
        [InlineData(1000000, 1024)]
        public void String_length_is_affected_by_parameter(int dictionarySize, int maxStringLength)
        {
            var dictionary = new RandomStringDictionary(Options.Create(new DictionaryOptions {DictionarySize = dictionarySize, MaxStringLength = maxStringLength}));
            Enumerable.Range(0, dictionarySize).Select(i => dictionary.GetItem(i)).Should().NotContain(s => s.Length > maxStringLength);
        }

        [Theory]
        [InlineData(9, 10)]
        [InlineData(0, 1)]
        [InlineData(3, 5)]
        public void Valid_index_test(int index, int dictionarySize)
        {
            var dictionary = new RandomStringDictionary(Options.Create(new DictionaryOptions {DictionarySize = dictionarySize, MaxStringLength = 100}));
            Action act = () => dictionary.GetItem(index);
            act.Should().NotThrow();
        }

        [Theory]
        [InlineData(100, 20)]
        [SuppressMessage("ReSharper", "PossibleMultipleEnumeration")]
        public void The_same_dictionary_returns_equal_values_for_the_index(int dictionarySize, int maxStringLength)
        {
            var dictionary = new RandomStringDictionary(Options.Create(new DictionaryOptions {DictionarySize = dictionarySize, MaxStringLength = maxStringLength}));
            var enumerable = Enumerable.Range(0, dictionarySize).Select(i => dictionary.GetItem(i));
            var firstPass = enumerable.ToList();
            var secondPass = enumerable.ToList();
            firstPass.Should().BeEquivalentTo(secondPass);
        }

        [Theory]
        [InlineData(100, 20)]
        public void Different_dictionaries_return_different_values_for_the_index(int dictionarySize, int maxStringLength)
        {
            var firstDict = new RandomStringDictionary(Options.Create(new DictionaryOptions {DictionarySize = dictionarySize, MaxStringLength = maxStringLength}));
            var firstPass = Enumerable.Range(0, dictionarySize).Select(i => firstDict.GetItem(i)).ToList();
            Thread.Sleep(TimeSpan.FromSeconds(1)); // Allowing private Random object to change the seed
            var secondDict = new RandomStringDictionary(Options.Create(new DictionaryOptions {DictionarySize = dictionarySize, MaxStringLength = maxStringLength}));
            var secondPass = Enumerable.Range(0, dictionarySize).Select(i => secondDict.GetItem(i)).ToList();
            firstPass.Should().NotBeEquivalentTo(secondPass);
        }
    }
}