# README #

Тестовое задания для компании Альтиум 

### Задание ###

На входе есть большой текстовый файл, где каждая строка имеет вид Number. String
Например:

    415. Apple
    30432. Something something something
    1. Apple
    32. Cherry is the best
    2. Banana is yellow
Обе части могут в пределах файла повторяться. Необходимо получить на выходе другой файл, где все строки отсортированы. Критерий сортировки: сначала сравнивается часть String, если она совпадает, тогда Number.
Т.е. в примере выше должно получиться

    1. Apple
    415. Apple
    2. Banana is yellow
    32. Cherry is the best
    30432. Something something something
Требуется написать две программы:
 1. Утилита для создания тестового файла заданного размера. Результатом работы должен быть текстовый файл описанного выше вида. Должно быть какое-то количество строк с одинаковой частью String.
2. Собственно сортировщик. Важный момент, файл может быть очень большой. Для тестирования будет использоваться размер ~100Gb.При оценке выполненного задания мы будем в первую очередь смотреть на результат (корректность генерации/сортировки и время работы), во вторую на то, как кандидат пишет код. Язык программирования: C#

Также согласованы следующие вопросы:
- возможно использование .Net Core 3
- консольный UI для обоих приложений
- строки не обязаны содержать осмысленные слова
- размер генерируемого файл не обязан с точностью до байта равным входному параметру
- кодировка файлов на усмотрение разработчика.

## Результат
Две программы, у обеих консольный интерфейс.

### Altium.HomeWork.Generator
- ofn имя выходного файла
- fs размер генерируемого файла в байтах
- ds размер словаря в словах
- ml максимальная длина случайных слов в словаре

пример: dotnet run -fs 10000000000 -ofn /Users/user/temp/output.txt

Размер словаря и размер файла связаны следующим образом: для обеспечения наличия повторов размер словаря умноженный на средний размер слова должен быть меньше размера выходного файла. Если это требование нарушено программа выдаст ошибку. Средний размер слова считается как среднее между минимальным и максимальным размером случайных слов, учитывая равную вероятность случайных чисел, генерируемых типом Random.
Выходной файл программа перезаписывает без предупреждений. 

### Altium.HomeWork.Sorter
- ifn имя входного файла от генератора
- ofn имя выходного файла
- mu размер используемой память. на самом деле это конечно ограничение на суммарный объем загруженных строк
- afe флаг который позволяет пропускать неправильные входные строки в файле. при включенном флаге плохие строки игнорируются, при выключенном останавливают процесс.

пример: dotnet run -mu 1000000000  -ifn /Users/user/Temp/output.txt -ofn /Users/user/Temp/sorted.txt

Алгоритм сортировки: external merge. входной файл разбивается на части допустимого размера, каждая часть сортируется в памяти и сохраняется в файл во временной директории. После чего все части собираются в один файл слиянием, а временные файлы удаляются.
Выходной файл так же перезаписывается без предупреждений.

## PS
Весь код рабочий, я тестировал его и на нескольких байтах и на 100Gb. Производительность сильно зависит от размера файла и количества строк (длина случайной строки).

В коде есть что поделать еще, тесты например, но мне кажется, это уже выходит за рамки небольшой тестовой задачи.

